From d64b73e0f9bcaa4106b1eb80e8d7231dc962d438 Mon Sep 17 00:00:00 2001
From: Daniel Kahn Gillmor <dkg@fifthhorseman.net>
Date: Thu, 8 Oct 2015 12:37:53 -0700
Subject: do-no-crash-long-salts

The OpenID 2.0 specification indicates that the response_nonce as a
whole can be up to 255 characters, and must be prefixed by an ISO-8601
timestamp in UTC:

https://openid.net/specs/openid-authentication-2_0.html#positive_assertions

even assuming the a very long timestamp, this suggests that the latter
part of the nonce could be over 200 characters long.

The current table definitions in sqlstore.py all assume that the nonce
should be 40 characters.  This causes a crash when used with existing
OpenID providers (e.g. the drupal openid_provider module generates
nonces with a 64-byte salt).

Note: this patch doesn't address in-place upgrades of existing
python-openid servers that use an sqlstore.  The right thing to do is
something like (in PostgreSQL, e.g.):

 ALTER TABLE %(nonces) ALTER COLUMN salt TYPE VARCHAR(255);

I don't see any database versioning or upgrade mechanisms, so it's not
clear how to apply this change dynamically (or to detect that it needs
to be applied).

Some sqlstore backends (sqlite?) may not be able to do an in-place
type change of a column.  Those backends may need to drop the nonces
table and recreate it.
Patch-Name: do-no-crash-long-salts.patch
---
 openid/store/sqlstore.py | 6 +++---
 1 file changed, 3 insertions(+), 3 deletions(-)

diff --git a/openid/store/sqlstore.py b/openid/store/sqlstore.py
index 58c4337..632644c 100644
--- a/openid/store/sqlstore.py
+++ b/openid/store/sqlstore.py
@@ -297,7 +297,7 @@ class SQLiteStore(SQLStore):
     CREATE TABLE %(nonces)s (
         server_url VARCHAR,
         timestamp INTEGER,
-        salt CHAR(40),
+        salt VARCHAR(255),
         UNIQUE(server_url, timestamp, salt)
     );
     """
@@ -376,7 +376,7 @@ class MySQLStore(SQLStore):
     CREATE TABLE %(nonces)s (
         server_url BLOB NOT NULL,
         timestamp INTEGER NOT NULL,
-        salt CHAR(40) NOT NULL,
+        salt VARCHAR(255) NOT NULL,
         PRIMARY KEY (server_url(255), timestamp, salt)
     )
     ENGINE=InnoDB;
@@ -447,7 +447,7 @@ class PostgreSQLStore(SQLStore):
     CREATE TABLE %(nonces)s (
         server_url VARCHAR(2047) NOT NULL,
         timestamp INTEGER NOT NULL,
-        salt CHAR(40) NOT NULL,
+        salt VARCHAR(255) NOT NULL,
         PRIMARY KEY (server_url, timestamp, salt)
     );
     """
